# Server Provision

A simple server provisioning role

## Requirements

Depending on the method chosen the requirements are as follows:

- Hezner
	- hcloud collection
	- python-hcloud
- KVM
	- many shits

## Role Variables

This role has a fair number of required vars. Most of them are best suited as group or host_vars 




## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }


## Example Inventory

```yaml
---
all:
  hosts:
    testeroni.jjlabs.dev:
      run: control
      host_state: present
      reconcile_subnets: true
      reconcile_ssh_keys: true
      subnets:
        - network_name: jj_cloud
          network_range: 10.0.0.0/8
          network_zone: eu-central
          network_state: present
          subnet_range: 10.0.0.0/24
          subnet_state: present
          subnet_type: cloud
          host_ip: 10.0.0.20
      hetzner_server_type: cx11
      provision_dest: hetzner
      hetzner_api_key: papi
      hetzner_location: fsn1
      hetzner_cloud_ssh_keys:
        - name: mykey
          state: present
          key: ssh-rsa AAAA
      hetzner_image: rocky-9
```

## License

MIT

